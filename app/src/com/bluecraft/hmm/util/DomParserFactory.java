//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Hidden Markov Model Library in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Hidden Markov Model Library in Java.
Please refer to Rabiner 1989.
All algorithms are directly taken from this article.
Notations and variable names also closely follow the conventions used in this paper.

@copyright  Hyoungsoo Yoon
@date  Feb 21st, 1999
*/
package com.bluecraft.hmm.util;


import org.w3c.dom.*;
import com.ibm.xml.parser.*;
import com.sun.xml.tree.*;

import java.io.*;
import java.util.*;
     

/**
DomParserFactory is a wrapper class for XML parsers.
This class exposes only DOM interfaces.
(e.g. SAX interfaces are hidden regardless of parsers used.)
Currently, only sun and ibm parsers are included
in order to facilitate easy distribution.

@author  Hyoungsoo Yoon
@version  0.3
*/
public class DomParserFactory implements XmlParserList {
    
    //<Static_Fields>
    final static boolean DEBUG = false;  // Not used at the moment.
    static String PARSER_NAME = null;
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>
    
    
    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    This is a "factory" class, and hence the constructor is private.
    */
    private DomParserFactory() {
    }
    //</Constructors>

    
    //<Private_Methods>
    //</Private_Methods>
    
    
    //<Protectd_Methods>
    //</Protectd_Methods>


    //<Public_Methods>
    /**
    Selects the parser to be used.
    Parser name is defined in the interface, XmlParserList.
    @param name Name of the parser to be created.
    */
    public static void setParser(String name) {
         PARSER_NAME = name;
    }

    /**
    Creates a new (empty) document.
    It calls the constructor of appropriate (sub)class
    according to the parser selected (PARSER_NAME).
    @return  org.w3c.dom.Document if successful, otherwise null.
    */
    public static Document newDocument() {
        Document doc = null;

        if(PARSER_NAME.equals(IBM_PARSER)) {
            doc = new TXDocument();
        } else if(PARSER_NAME.equals(SUN_PARSER)) {
            doc = new XmlDocument();
        } else {
	    // Only two parsers are supported at the moment.
            System.err.println("Parser name not recognized!");
        }

        return doc;
    }

    /**
    Creates a new (empty) document with the given file name.
    It calls the constructor of appropriate (sub)class
    according to the parser selected (PARSER_NAME).
    @param  fileName name of an XML file to be associated with the document.
    @return  org.w3c.dom.Document if successful, otherwise null.
    */
    public static Document newLocalDocument(String fileName) {
        Document doc = null;

        if(fileName == null) {
            return newDocument();
        }

        if(PARSER_NAME.equals(IBM_PARSER)) {
            // Not implemented yet.
        } else if(PARSER_NAME.equals(SUN_PARSER)) {
            // Not implemented yet.
        } else {
	    // Only two parsers are supported at the moment.
            System.err.println("Parser name not recognized!");
        }

        return doc;
    }


    /**
    Opens a document with the given file name.
    It calls the constructor of appropriate (sub)class
    according to the parser selected (PARSER_NAME).
    @param  fileName name of an XML file to be associated with the document.
    @return  org.w3c.dom.Document if successful, otherwise null.
    */
    public static Document openLocalDocument(String fileName) {
        Document doc = null;

        if(PARSER_NAME.equals(IBM_PARSER)) {
            if (fileName != null) {
                InputStream is;
                try {
                    is = new FileInputStream(fileName);
                } catch (FileNotFoundException notFound) {
                    System.err.println(notFound);
                    return null;
                }

                Parser parser = new Parser(fileName);
                // Set some parser options here
                parser.setPreserveSpace(false);
                parser.setKeepComment(false);
                doc = parser.readStream(is);
            } else {
               // Not implemented yet.
            }
        } else if(PARSER_NAME.equals(SUN_PARSER)) {
            if (fileName != null) {
                try {
                    // turn the filename into a fully qualified URL
                    String uri = "file:" + new File(fileName).getAbsolutePath();
                    doc = XmlDocumentBuilder.createXmlDocument(uri);
               } catch(Exception e) {
                   e.printStackTrace();
               }
            } else {
               // Not implemented yet.
            }
        } else {
            // Only two parsers are supported at the moment.
            System.err.println("Parser name not recognized!");
        }

        return doc;
    }
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    First, it sets the parser,
    then opens a local document,
    and finally perfoms operations defined by DOM interfaces.
    No error checking is done.
    */
    public static void main(String[] args) {

        DomParserFactory.setParser(SUN_PARSER);
        Document doc = DomParserFactory.openLocalDocument("hmm.xml");
        
        NodeList listOfPersons = 
            doc.getElementsByTagName( "element" );
        int numberOfPersons = listOfPersons.getLength();

        System.out.println("Number: " + numberOfPersons);
    }
    //</Main_Method>
}
