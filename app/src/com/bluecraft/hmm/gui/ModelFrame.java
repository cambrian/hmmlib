//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Hidden Markov Model Library in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Hidden Markov Model Library in Java.
Please refer to Rabiner 1989.
All algorithms are directly taken from this article.
Notations and variable names also closely follow the conventions used in this paper.

@copyright  Hyoungsoo Yoon
@date  Feb 21st, 1999
*/
package com.bluecraft.hmm.gui;


import com.bluecraft.hmm.util.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
     
     
/**
Main frame window for Hidden Markov Model Library in Java

@author  Hyoungsoo Yoon
@version  0.3
*/
public class ModelFrame extends JFrame 
    implements ActionListener, ItemListener {
    
    final static boolean DEBUG = false;

    protected JTextArea textArea;
    protected String newline = "\n";
    
    private boolean bToolBarWithText = true;
    
    protected Action newFileAction;
    protected Action openFileAction;
    protected Action saveFileAction;
    protected Action exitAction;
    protected Action stepForwardAction;
    protected Action stepBackwardAction;
    protected Action leftAction;
    protected Action middleAction;
    protected Action rightAction;
    
    
    
    public ModelFrame() {
        this("Model Frame");
    }    

    public ModelFrame(String frameTitle) {
        //Do frame stuff.
        super(frameTitle);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        //Create the toolbar and menu.
        JToolBar toolBar = new JToolBar();
        JMenu fileMenu = createFileMenu(toolBar);
        JMenu modelMenu = createModelMenu(toolBar);
        JMenu lookFeelMenu = createLookFeelMenu();

        //Create the text area used for output.
        textArea = new JTextArea(5, 30);
        JScrollPane scrollPane = new JScrollPane(textArea);
        
        //Lay out the model pane.
        JPanel modelPane = new ModelPanel();
        
        //Lay out the main pane.
        JPanel mainPane = new JPanel();
        mainPane.setLayout(new BorderLayout());
        mainPane.setPreferredSize(new Dimension(400, 300));
        mainPane.add(modelPane, BorderLayout.CENTER);
        mainPane.add(scrollPane, BorderLayout.SOUTH);
        
        //Lay out the content pane.
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.setPreferredSize(new Dimension(450, 350));
        contentPane.add(toolBar, BorderLayout.NORTH);
        contentPane.add(mainPane, BorderLayout.CENTER);
        
        // Set content pane
        setContentPane(contentPane);
        

        //Set up the menu bar.
        JMenuBar mb = new JMenuBar();
        mb.add(fileMenu);
        mb.add(modelMenu);
        mb.add(lookFeelMenu);
        setJMenuBar(mb);
    }

    // file Menu 
    protected JMenu createFileMenu(JToolBar toolBar) {
    
        JMenu fileMenu = new JMenu("File");

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;

        // New file
        imgIcon = new ImageIcon("images/dumb.gif");
        newFileAction = new AbstractAction("New", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                displayResult("Action for New File", e);
            }
        };
        button = toolBar.add(newFileAction);
        if(bToolBarWithText) {
            button.setText("New");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("New File");
        menuItem = fileMenu.add(newFileAction);
        menuItem.setIcon(imgIcon);

        // Open file
        imgIcon = new ImageIcon("images/dumb.gif");
        openFileAction = new AbstractAction("Open...", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                displayResult("Action for Open File", e);
            }
        };
        button = toolBar.add(openFileAction);
        if(bToolBarWithText) {
            button.setText("Open");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Open File");
        menuItem = fileMenu.add(openFileAction);
        menuItem.setIcon(imgIcon);
        
        // Save file
        imgIcon = new ImageIcon("images/dumb.gif");
        saveFileAction = new AbstractAction("Save", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                displayResult("Action for Save File", e);
            }
        };
        button = toolBar.add(saveFileAction);
        if(bToolBarWithText) {
            button.setText("Save");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Save File");
        menuItem = fileMenu.add(saveFileAction);
        menuItem.setIcon(imgIcon);
        
        fileMenu.addSeparator();

        // Exit
        imgIcon = new ImageIcon("images/dumb.gif");
        exitAction = new AbstractAction("Exit", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
                //displayResult("Action for Exit", e);
            }
        };
                
        menuItem = fileMenu.add(exitAction);
        menuItem.setIcon(null);
        
        return fileMenu;
    }


    // model Menu 
    protected JMenu createModelMenu(JToolBar toolBar) {
    
        JMenu modelMenu = new JMenu("Model");

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;

        // Step Forward
        imgIcon = new ImageIcon("images/right.gif");
        stepForwardAction = new AbstractAction("Step Forward", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                stepBackwardAction.setEnabled(true);
                //displayResult("Action for Step Forward", e);
            }
        };
        button = toolBar.add(stepForwardAction);
        if(bToolBarWithText) {
            button.setText("Forward");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Step Forward");
        menuItem = modelMenu.add(stepForwardAction);
        menuItem.setIcon(imgIcon);
        
        // Step Backward
        imgIcon = new ImageIcon("images/left.gif");
        stepBackwardAction = new AbstractAction("Step Backward", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                stepBackwardAction.setEnabled(false);
                //displayResult("Action for Step Backward", e);
            }
        };
        button = toolBar.add(stepBackwardAction);
        if(bToolBarWithText) {
            button.setText("Backward");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Step Backward");
        menuItem = modelMenu.add(stepBackwardAction);
        menuItem.setIcon(imgIcon);
        
        return modelMenu;
    }


    protected JMenu createLookFeelMenu() {
        JMenu lfMenu = new JMenu("Look/Feel");
        
        //a group of radio button menu items
        ButtonGroup group = new ButtonGroup();
        JRadioButtonMenuItem rbMenuItem = null;
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.METAL_LF_NAME);
        rbMenuItem.setSelected(true);  // Meta LF is the default
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(ModelFrame.this, LookFeel.METAL_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);
        
        lfMenu.addSeparator();
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.WINDOWS_LF_NAME);
        rbMenuItem.setSelected(false);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(ModelFrame.this, LookFeel.WINDOWS_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.MOTIF_LF_NAME);
        rbMenuItem.setSelected(false);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(ModelFrame.this, LookFeel.MOTIF_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);
        
        // Mac Look and Feel not available!
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.MAC_LF_NAME);
        rbMenuItem.setSelected(false);
        rbMenuItem.setEnabled(false);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(this);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(ModelFrame.this, LookFeel.MAC_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);

        return lfMenu;
    }

    protected void displayResult(String actionDescription, ActionEvent e) {
        String s = ("Action event detected by: " + actionDescription
                  + newline 
                  + "    Event source: " + e.getSource()
                  + newline);
        textArea.append(s);
    }


    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
                   + newline
                   + "    Event source: " + source.getText()
                   + " (an instance of " + getClassName(source) + ")";
        textArea.append(s + newline);
    }

    public void itemStateChanged(ItemEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Item event detected."
                   + newline
                   + "    Event source: " + source.getText()
                   + " (an instance of " + getClassName(source) + ")"
                   + newline
                   + "    New state: " 
                   + ((e.getStateChange() == ItemEvent.SELECTED) ?
                     "selected":"unselected");
        textArea.append(s + newline);
    }

    // Returns just the class name -- no package info.
    protected String getClassName(Object o) {
        String classString = o.getClass().getName();
        int dotIndex = classString.lastIndexOf(".");
        return classString.substring(dotIndex+1);
    }



    public static void main(String[] args) {

        LookFeel.initializeLF();
        
        ModelFrame frame = new ModelFrame();
        
        frame.setTitle("Model Frame Demo");
        frame.setSize(450, 350);
        frame.pack();
        frame.setVisible(true);
    }
}	
