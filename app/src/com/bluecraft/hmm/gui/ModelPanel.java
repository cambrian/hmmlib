//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Hidden Markov Model Library in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/**
Hidden Markov Model Library in Java.
Please refer to Rabiner 1989.
All algorithms are directly taken from this article.
Notations and variable names also closely follow the conventions used in this paper.

@copyright  Hyoungsoo Yoon
@date  Feb 21st, 1999
*/
package com.bluecraft.hmm.gui;


import com.bluecraft.hmm.util.*;
import com.bluecraft.hmm.Model;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;


/**
Main panel window for Hidden Markov Model Library in Java

@author  Hyoungsoo Yoon
@version  0.3
*/
public class ModelPanel extends JPanel {

    final static String LINEARVIEW = "Linear View";
    final static String CIRCULARVIEW = "Circular View";
    private String comboBoxItems[] = { LINEARVIEW, CIRCULARVIEW };
    
    private JPanel viewPane = new JPanel();
    private JTextField outputField = null;
    private Model model = null;
    
    
    public ModelPanel() {
    
        //Dimension buttonSize = new Dimension(130,25);
        
        // combo box
        JComboBox cBox = new JComboBox(comboBoxItems);
        //cBox.setPreferredSize(buttonSize);
        //cBox.setMinimumSize(buttonSize);
        cBox.setEditable(false);
        //cBox.addItemListener(this);
        cBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                CardLayout cl = (CardLayout)(viewPane.getLayout());
                cl.show(viewPane, (String)evt.getItem());
            }
        });      
        
        // the first button.
        JButton firstButton = new JButton("First"); 
        //firstButton.setPreferredSize(buttonSize);
        //firstButton.setMinimumSize(buttonSize);
        firstButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
	      });
	
        // the second button.
        JButton secondButton = new JButton("Second"); 
        //secondButton.setPreferredSize(buttonSize);
        //secondButton.setMinimumSize(buttonSize);
        secondButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });


        // Layout buttons in a column
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        JPanel tPane = new JPanel();
        tPane.setLayout(new GridLayout(2,1,2,2));
        JLabel tLabel = new JLabel("Model View");
        tPane.add(tLabel);
        tPane.add(cBox);
        buttonPane.add(tPane);
        buttonPane.add(Box.createRigidArea(new Dimension(0,5)));
        JPanel bPane = new JPanel();
        bPane.setLayout(new GridLayout(20,1,2,2));
        JLabel bLabel = new JLabel("Operations");
        bPane.add(bLabel);
        bPane.add(firstButton);
        bPane.add(secondButton);
        buttonPane.add(bPane);
        
	
        // Create Linear Model View panes
        LinearModelView lineModelView = new LinearModelView();



        // Create Circular Model View panes
        CircularModelView circModelView = new CircularModelView();



        //Layout the view pane (inside scrollPane).
        viewPane.setLayout(new CardLayout());
        viewPane.setPreferredSize(new Dimension(500, 400));
        viewPane.add(LINEARVIEW, lineModelView);
        viewPane.add(CIRCULARVIEW, circModelView);
        JScrollPane scrollPane = new JScrollPane(viewPane);
        
        //Layout the graph pane.
        JPanel graphPane = new JPanel();
        graphPane.setLayout(new BorderLayout());
        graphPane.setPreferredSize(new Dimension(450, 350));
        graphPane.add(scrollPane, BorderLayout.CENTER);
        outputField = new JTextField("Output Sequence...");
        outputField.setEditable(false);
        graphPane.add(outputField, BorderLayout.SOUTH);
        
        
        //Layout the model pane.
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(450, 350));
        add(buttonPane, BorderLayout.WEST);
        add(graphPane, BorderLayout.CENTER);

    }
    
    
    
    

    public void initializeModel() {
        model = new Model();
    }
    


    /**
    @return  Number of states in the Markov model 
    */
    public int getNumStates() {
        return model.getNumStates();
    }
    /**
    @return  Number of observable symbols 
    */
    public int getNumSymbols() {
        return model.getNumSymbols();
    }









    public static void main(String[] args) {

        LookFeel.initializeLF();
        
        JFrame frame = new JFrame();
        frame.getContentPane().add(new ModelPanel());
        
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        frame.setTitle("Model Panel Demo");
        frame.setSize(550, 450);
        frame.pack();
        frame.setVisible(true);
    }

}
