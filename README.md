# HMMLib

Hidden Markov Model (HMM) is a simple statistical model, for modeling a time series or other stochastic events.

HMM used to be _the_ model for speech recognition.
HMMLib is a HMM library written in Java.

These days, the deep learning performance, in speech recognition, is proven to be so much better than that of HMM,
and its usefulness has somewhat decreased.

HMMLib was released ~15 years ago as open source (on SourceForge).



